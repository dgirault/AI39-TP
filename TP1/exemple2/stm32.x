
OUTPUT_FORMAT("elf32-littlearm", "elf32-bigarm", "elf32-littlearm")
OUTPUT_ARCH(arm)
ENTRY(main)
SECTIONS {
 . = 0x00000011;
 .text : { 
  *(.text) 
  *(.rodata)
  . = ALIGN(4);
 }

 .data : {
  *(.data);
 }

 .bss : {
  *(.bss)
  *(COMMON);
 }
}
