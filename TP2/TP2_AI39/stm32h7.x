
OUTPUT_FORMAT("elf32-littlearm", "elf32-bigarm", "elf32-littlearm")
OUTPUT_ARCH(arm)
ENTRY(_init)

MEMORY {
	ITCM (rwx) : ORIGIN = 0, LENGTH = 64K
	DTCM (rw) : ORIGIN = 0x2000000, LENGTH = 128K
	FLASH (rx) : ORIGIN = 0x8000000, LENGTH = 2M
}

SECTIONS {
 
 .text : { 
  *(.vect)
  *(.text) 
  *(.rodata)
  . = ALIGN(4);
 } > FLASH
 
 
 .data : {
  *(.data);
 } > ITCM AT> FLASH
 
 start_flash = LOADADDR(.data);
 end_flash = LOADADDR(.data) + SIZEOF(.data);
 
  start_itcm = ADDR(.data);
 end_itcm = ADDR(.data) + SIZEOF(.data);
 
 debut = .;
 .bss : {
  *(.bss)
  *(COMMON);
 } > DTCM
 fin = .;
}
