#include <stdint.h>
#include "biblio.h"

/* Initialiser un port donné.
     Le bit est initialisé en entrée ou sortie en fonction de dir :
       - 0 : sortie
       - 1 : entrée
     Le port est identifié par une lettre de 'A' à 'K'
     Le bit par un numéro d'ordre de 0 à 15.
  */
  void gpio_init(char port, int bit, int dir) {
	  volatile periph_t* led = (periph_t*) (0x58020000 + (port-0x41)*0x400);
	  // configuratio
	  if (dir==1)	//output
		  led->moder |= (1 << (bit*2) );
	  else//input
		  led->moder &= 0b0111111111111111111111111;

	  //led->moder = (led->moder & ~((uint32_t)1 << bit)) | ( (uint32_t)dir << bit );
  }

  /* Affecter un bit à un port configuré en sortie */
  void gpio_set(char port, int n, int value) {
	  volatile periph_t* led = (periph_t*) (0x58020000 + (port-0x41)*0x400);

	  if (value==0)	{
	  	  uint32_t mask = (0b1 << n);
	  	  int number = 1;
	  	  for(int i=0; i<n; i++) { number *= 2; }
		  mask ^= number; // reverse mask
		  led->odr &= mask;
	  }
	  else {
		  led->odr |= (0b1 << n);
	  }
  }

  /* Lire un bit d'un port configuré en entrée */
  int gpio_get(char port, int bit) {
	  return 1;
  }

  /* Inverser la valeur d'une entrée */
  void gpio_toggle(char port, int bit) {

  }
