#include <stdint.h>

#include "biblio.h"

#define LOOP 20000
int main(void) 
{

	/* initialisation */

    volatile uint32_t* periph_RCC = (uint32_t*) (0x58024400 + 0x0E0);

     *periph_RCC |= 0b100000000;



    while( !((*periph_RCC & (0b1 << 8))>>8)) {
    }

    *periph_RCC;








    // Tim2

    volatile uint32_t* periph_Tim2 = (uint32_t*) (0x58024400 + 0x0E8);

	*periph_Tim2 = *periph_Tim2 | 0b1;

    while( !((*periph_Tim2 & 0b1))) {
    }

	*periph_Tim2;

	//  deactivate tim2
	volatile uint32_t* counterConfig = (uint32_t*) (0x40000000);
	//*counterConfig &= ~(1);

	//activation
	*counterConfig |= 0b1;

	// Configure timer
	volatile uint32_t* counterPSC = (uint32_t*) (0x40000000 + 0x28);
	*counterPSC = 63;

	// COnfigure maxValue
	volatile uint32_t* counterValueMax = (uint32_t*) (0x40000000 + 0x2C);
	*counterValueMax = LOOP;



	volatile uint32_t* counterUpdate = (uint32_t*) (0x40000000 + 0x10);

	volatile uint32_t* counterValue = (uint32_t*) (0x40000000 + 0x24);
	*counterValue=0;


    // test led orange
    volatile periph_t* led_orange = (periph_t*) (0x58022000);

    // configuration output
    led_orange->moder |= (1 << 24);

    // gpio_init('I', 13, 1);
    *counterValue=0;
    while(1) {
		while (!(*counterUpdate & 0b1)) {
		}
		*counterUpdate = 0b0;

		led_orange->odr &= 0b011111111111111;
    	// gpio_set('I', 13, 0);

		while (!(*counterUpdate & 0b1)) {}
		*counterUpdate = 0b0;

		led_orange->odr |= (0b1 << 14);
    	// gpio_set('I', 13, 1);
    }
    return 0;
}
