

typedef struct __attribute__((packed, aligned(4)))

{
	volatile uint32_t moder;
	volatile uint32_t reserved1[3];
	volatile uint32_t idr;
	volatile uint32_t odr;
} periph_t;

	/* Initialiser un port donné.
     Le bit est initialisé en entrée ou sortie en fonction de dir :
       - 0 : sortie
       - 1 : entrée
     Le port est identifié par une lettre de 'A' à 'K'
     Le bit par un numéro d'ordre de 0 à 15.
  */
  void gpio_init(char port, int bit, int dir);

  /* Affecter un bit à un port configuré en sortie */
  void gpio_set(char port, int n, int value);

  /* Lire un bit d'un port configuré en entrée */
  int gpio_get(char port, int bit);

  /* Inverser la valeur d'une entrée */
  void gpio_toggle(char port, int bit);
