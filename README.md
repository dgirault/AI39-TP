# AI39

## Author
* Dylan GIRAULT
* Etienne CLAIRIS

## Purpose
* Stockage des activités du TP ARM en AI39

## Navigation
* L'accès au produit final de l'activité se situe à [AI39/TP2/TP2_AI39](https://gitlab.utc.fr/dgirault/AI39-TP/-/tree/main/TP2/TP2_AI39?ref_type=heads)
* Il est prévu de l'utiliser avec l'IDE Eclipse